<?php
require_once(__DIR__ . '/lib/db.php');
require_once(__DIR__ . '/lib/modules.php');
require_once(__DIR__ . '/lib/template.php');

$distributions = Sixdoc\fetch_module_index($sixdoc_db);
Sixdoc\template([['Index', '/']], function() use($distributions) {
    ?>
        <ul>
            <? foreach ($distributions as $distribution => $modules): ?>
                <li>
                    <a href="<?= htmlentities(Sixdoc\distribution_url($distribution)) ?>">
                        <strong><?= htmlentities($distribution) ?></strong>
                    </a>
                    <ul>
                        <? foreach ($modules as $module): ?>
                            <li>
                                <a href="<?= htmlentities(Sixdoc\module_url($distribution, $module)) ?>">
                                    <?= htmlentities($module) ?>
                                </a>
                            </li>
                        <? endforeach; ?>
                    </ul>
                </li>
            <? endforeach; ?>
        </ul>
    <?php
});
