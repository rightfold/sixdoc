<?php
namespace Sixdoc;

function fetch_module_index($db) {
    $rows = $db->query('
        SELECT name, distribution_name
        FROM modules
        ORDER BY distribution_name COLLATE NOCASE ASC
               , name COLLATE NOCASE ASC
    ');
    $distributions = [];
    foreach ($rows as $row) {
        if (!array_key_exists($row->distribution_name, $distributions)) {
            $distributions[$row->distribution_name] = [];
        }
        $distributions[$row->distribution_name][] = $row->name;
    }
    return $distributions;
}

function fetch_module($db, $distribution_name, $name) {
    $query = $db->prepare('
        SELECT distribution_name, name, html
        FROM modules
        WHERE distribution_name = :distribution_name
        AND name = :name
    ');
    $query->execute([':distribution_name' => $distribution_name, ':name' => $name]);
    $rows = $query->fetchall();
    return count($rows) === 0 ? NULL : $rows[0];
}

function fetch_distribution($db, $name) {
    $query = $db->prepare('
        SELECT name, source_url
        FROM distributions
        WHERE name = :name
    ');
    $query->execute([':name' => $name]);
    $rows = $query->fetchall();
    if (count($rows) === 0) {
        return NULL;
    }
    $distribution = $rows[0];
    $distribution->modules = fetch_module_index($db)[$distribution->name];
    return $distribution;
}

function distribution_url($distribution_name) {
    return '/distribution.php?name=' . urlencode($distribution_name);
}

function module_url($distribution_name, $name) {
    return '/module.php'
        . '?distribution_name=' . urlencode($distribution_name)
        . '&name=' . urlencode($name);
}

function echo_html_body($html) {
    $found_body = FALSE;
    foreach (explode("\n", $html) as $line) {
        if (!$found_body) {
            $found_body = preg_match('/^<body /', $line);
            continue;
        }
        echo "$line\n";
        if (preg_match('/^<\/body>/', $line)) {
            break;
        }
    }
}
