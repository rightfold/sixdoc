<?php
namespace Sixdoc;

function template($breadcrumb, $body) {
    ?>
        <!doctype html>
        <html id="___top">
            <head>
                <meta charset='utf-8'>
                <title>
                    <? foreach (array_reverse($breadcrumb) as $entry): ?>
                        <?= htmlentities($entry[0]) ?> |
                    <? endforeach; ?>
                    Sixdoc
                </title>
                <link rel="stylesheet" href="/static/reset.css">
                <link rel="stylesheet" href="/static/sixdoc.css">
            </head>
            <body>
                <div class="sixdoc-page">
                    <ul class="sixdoc-breadcrumb">
                        <? foreach ($breadcrumb as $entry): ?>
                            <li>
                                <a href="<?= htmlentities($entry[1]) ?>">
                                    <?= htmlentities($entry[0]) ?></a>
                            </li>
                        <? endforeach; ?>
                    </ul>
                    <h1><?= htmlentities($entry[0]) ?></h1>
                    <? $body() ?>
                </div>
                <footer>
                    <p class="sixdoc-copyright">
                        Sixdoc &copy; rightfold and contributors. For copyright
                        information of hosted documentation, see their
                        respective license files.
                    </p>
                </footer>
            </body>
        </html>
    <?php
}
