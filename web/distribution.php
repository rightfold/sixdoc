<?php
require_once(__DIR__ . '/lib/db.php');
require_once(__DIR__ . '/lib/modules.php');
require_once(__DIR__ . '/lib/template.php');

$distribution = Sixdoc\fetch_distribution($sixdoc_db, $_GET['name']);
if ($distribution === NULL) {
    die('not found');
}

$breadcrumb = [[$distribution->name, Sixdoc\distribution_url($distribution->name)]];
Sixdoc\template($breadcrumb, function() use($distribution) {
    ?>
        <article>
            <table>
                <tr>
                    <th>Source URL</th>
                    <td><code><?= htmlentities($distribution->source_url) ?></code></td>
                </tr>
            </table>

            <h1>Installation</h1>
            <pre>panda install <?= htmlentities($distribution->name) ?></pre>

            <h1>Modules</h1>
            <ul>
                <? foreach ($distribution->modules as $module): ?>
                    <li>
                        <a href="<?= htmlentities(Sixdoc\module_url($distribution->name, $module)) ?>">
                            <?= htmlentities($module) ?>
                        </a>
                    </li>
                <? endforeach; ?>
            </ul>
        </article>
    <?php
});
