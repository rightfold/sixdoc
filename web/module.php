<?php
require_once(__DIR__ . '/lib/db.php');
require_once(__DIR__ . '/lib/modules.php');
require_once(__DIR__ . '/lib/template.php');

$module = Sixdoc\fetch_module($sixdoc_db, $_GET['distribution_name'], $_GET['name']);
if ($module === NULL) {
    die('not found');
}

$breadcrumb = [
    [$module->distribution_name, Sixdoc\distribution_url($module->distribution_name)],
    [$module->name, Sixdoc\module_url($module->distribution_name, $module->name)],
];
Sixdoc\template($breadcrumb, function() use($module) {
    ?>
        <article>
            <? Sixdoc\echo_html_body($module->html) ?>
        </article>
    <?php
});
